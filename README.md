Horror Ambience
===============

Make night darker.

Usage
-----

Always carry your torch on.

Installation
------------

Move the `horror-ambience-master` folder to 7 Days to Die's mod folder.<br>

- Steam:
```shell
SteamLibrary/steamapps/common/7 Days to Die/Mods
```

If there is no `Mods` folder, create one there.

Repositories
------------

- https://gitlab.com/moredaystodie/horror-ambience (master)
- https://github.com/KanuX-14/MDTD-Horror-Ambience (mirror)

References
----------

This mod is based on Doughphunghus's modlets [Dark Nights](https://7daystodiemods.com/dark-nights/) and [Dark Indoors](https://7daystodiemods.com/dark-indoors/).

